## About the Extension ##
This extension is an SEO fix for homepages  of Magento Sitemap.xml files. It will remove the CMS page URL key, so that the page will point to the site base URL.

This extension is compatible for both CE and EE versions from 1.7.x on wards. Please read my blog post for more details.
URL: http://thecoderin.com/magento/sitemap-xml-fix-homepage-url

## Installation instructions ##

* Download the source from the Repository.
Note: Please disable the compilation process before installing the extension. You can enable back once the installation is complete.
* Copy the contents in the appropriate folder.
* Clear the Magento Caches.
* Regenerate sitemap from Magento Admin [Catalog/Google Sitemap].

## Extension Info and details ##
* Developed By: Anish Karim
* Email: thecoderinATgmailDOTcom
* Version: 0.1.0
* License: GPL-3.0 [http://opensource.org/licenses/gpl-3.0.html]
* Last Updated: February 17, 2015
